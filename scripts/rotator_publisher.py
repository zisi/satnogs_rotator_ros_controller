#!/usr/bin/env python

import rospy
from dynamic_reconfigure.server import Server
from satnogs_rotator_controller.cfg import rotator_parametersConfig
from hamlib_rotator import rotator
from sensor_msgs.msg import JointState, Temperature
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus
from time import sleep


def callback(config, level):
    if (config.command == 'P'):
        satnogs_rot.position = (config.position_az, config.position_el)
    elif (config.command == 'M'):
        # Convert to mdeg/s
        if (config.speed_az >= 0):
            satnogs_rot.move("ROT_MOVE_LEFT", int(1000 * config.speed_az))
        else:
            satnogs_rot.move("ROT_MOVE_RIGHT", int(1000 * config.speed_az))
        sleep(0.1)  # Time in seconds
        if (config.speed_el >= 0):
            satnogs_rot.move("ROT_MOVE_UP", int(1000 * config.speed_el))
        else:
            satnogs_rot.move("ROT_MOVE_DOWN", int(1000 * config.speed_el))
    elif (config.command == 'S'):
        satnogs_rot.stop()
    elif (config.command == 'R'):
        satnogs_rot.reset()
    return config


def rot_status_code(status_code):
    switcher = {
        0: 'None',
        1: 'IDLE',
        2: 'MOVING',
        4: 'POINTING',
        8: 'ERROR'
        }
    return switcher.get(status_code, 'Invalid status code')


def rot_error_code(error_code):
    switcher = {
        0: 'None',
        1: 'NO ERROR',
        2: 'SENSOR ERROR',
        4: 'HOMING ERROR',
        8: 'MOTOR ERROR',
        12: 'OVER TEMPERATURE',
        16: 'WDT ERROR'
        }
    return switcher.get(error_code, 'Invalid error code')


if __name__ == '__main__':
    rospy.init_node('rotator_publisher', anonymous=True)
    # Set parameters
    rot_model = rospy.get_param('/rotator_node/rot_model')
    rot_serial_rate = rospy.get_param('/rotator_node/rot_serial_rate')
    rot_port = rospy.get_param('/rotator_node/rot_port')
    rot_board = rospy.get_param('/rotator_node/rot_board')
    satnogs_rot = rotator(rot_model, rot_serial_rate, rot_port)
    satnogs_rot.open()

    # Definitions for get configuration for e.g SatNOGS board
    if (rot_board == 'satnogs'):
        read_input_cfg = 6
        get_status_cfg = 3
        get_error_cfg = 4
        request_version_cfg = 5
    elif (rot_board == 'dummy'):
        read_input_cfg = 0
        get_status_cfg = 0
        get_error_cfg = 0
        request_version_cfg = 0

    # Set the type of messages
    rot_temperature_msg = Temperature()
    rot_diagnostic_msg = DiagnosticArray()
    rot_joint_msg = JointState()

    try:
        srv = Server(rotator_parametersConfig, callback)
        # Set the topics to publish data
        pub_joint = rospy.Publisher('rot_joint', JointState, queue_size=100)
        pub_temp = rospy.Publisher('rot_temperature', Temperature,
                                   queue_size=100)
        pub_diag = rospy.Publisher('diagnostics', DiagnosticArray,
                                   queue_size=100)
        rate = rospy.Rate(10)

        while not rospy.is_shutdown():
            # Get position
            rot_joint_msg.header.stamp = rospy.Time.now()
            rot_joint_msg.position = satnogs_rot.position
            sleep(0.1)  # Time in seconds
            # Get temperature, end-stops status and velocities
            rot_temperature_msg.header.stamp = rospy.Time.now()
            rot_value_cfg = satnogs_rot.get_conf(read_input_cfg)
            if (rot_value_cfg):
                rot_value_cfg = rot_value_cfg.split(',')
                rot_temperature = int(rot_value_cfg[0])
                rot_end_stop_status = [str(rot_value_cfg[1]),
                                       str(rot_value_cfg[2])]
                rot_velocities = [float(rot_value_cfg[3]),
                                  float(rot_value_cfg[4])]
            else:
                rot_temperature = 0
                rot_end_stop_status = ['0', '0']
                rot_velocities = [0.0, 0.0]
            sleep(0.1)  # Time in seconds
            # Get rotator status
            rot_diagnostic_msg.header.stamp = rospy.Time.now()
            rot_status_cfg = satnogs_rot.get_conf(get_status_cfg)
            if (rot_status_cfg):
                rot_status_cfg = rot_status_cfg.split(',')
                rot_status = int(rot_status_cfg[0])
            else:
                rot_status = 0
            sleep(0.1)  # Time in seconds
            # Get rotator error
            rot_error_cfg = satnogs_rot.get_conf(get_error_cfg)
            if (rot_error_cfg):
                rot_error_cfg = rot_error_cfg.split(',')
                rot_error = int(rot_error_cfg[0])
            else:
                rot_error = 0
            sleep(0.1)  # Time in seconds
            # Get rotator version
            rot_version_cfg = satnogs_rot.get_conf(request_version_cfg)
            if (rot_version_cfg):
                rot_version_cfg = rot_version_cfg.split(',')
                rot_version = str(rot_version_cfg[0])
            else:
                rot_version = '0'

            # Populate messages
            rot_joint_msg.name = ['AZ', 'EL']
            rot_joint_msg.velocity = rot_velocities
            rot_joint_msg.effort = [0.0, 0.0]
            rot_temperature_msg.temperature = rot_temperature
            rot_temperature_msg.variance = 1.0
            # Diagnostics
            az_switch = DiagnosticStatus(name='Azimuth Switch',
                                         level=0,
                                         message=rot_end_stop_status[0],
                                         hardware_id=rot_version)
            el_switch = DiagnosticStatus(name='Elevation Switch',
                                         level=0,
                                         message=rot_end_stop_status[1],
                                         hardware_id=rot_version)
            if (rot_status == 1 or rot_status == 2 or rot_status == 4):
                rot_status_level = 0
            else:
                rot_status_level = 2
            rot_status = DiagnosticStatus(name='Rotator Status',
                                          level=rot_status_level,
                                          message=rot_status_code(rot_status),
                                          hardware_id=rot_version)
            if (rot_error == 1):
                rot_error_level = 0
            else:
                rot_error_level = 2
            rot_error = DiagnosticStatus(name='Rotator Error',
                                         level=rot_error_level,
                                         message=rot_error_code(rot_error),
                                         hardware_id=rot_version)
            rot_diagnostic_msg.status = [az_switch, el_switch, rot_status,
                                         rot_error]
            # Publish messages
            pub_joint.publish(rot_joint_msg)
            pub_temp.publish(rot_temperature_msg)
            pub_diag.publish(rot_diagnostic_msg)
            rate.sleep()
    except rospy.ROSInterruptException:
        pass
