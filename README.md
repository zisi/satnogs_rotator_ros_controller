Licensed under the [GPLv3](LICENSE)## SatNOGS Rotator Controller - ROS
This repository uses python bindings of 
[Hamlib 3.3](https://github.com/Hamlib/Hamlib) in order 
to communicate with [SatNOGS Rotator 
Controller](https://wiki.satnogs.org/SatNOGS_Rotator_Controller).
This ROS package tested in ubuntu 18.10 with ROS lunar 1.14.2.

## Installation
To install the ROS package, clone the repository files in your ROS workspace:
```bash
roscd
cd ../src
git clone "..."
cd ..
catkin_make #Build the package
```
## Run
To run the node, run the launch file:
```
roslaunch satnogs_rotator_controller rotator.launch
```
Before you running the launch file you must put the correct ros parameters
in launch file, like:
* rot_model, the rotator model according to hamlib
* rot_serial_rate, the serial communication rate
* rot_port, the serial port
* rot_board, satnogs for SatNOGS v2.2, dummy for Dummy rotator

The implemented commands are:
* P, for set position (like hamlib)
* M, for set velocity (like hamlib)
* S, to stop the rotator (like hamlib)
* R, to reset the rotator (Home position, like Halib)

The rqt_plot is shown sensor-msg topic. Another example of
rqt-reconfigure is shown this [ros-package](https://gitlab.com/zisi/SuperModifiedServo-ROS)

![Run Launch file](https://gitlab.com/zisi/satnogs_rotator_ros_controller/uploads/5ff048e0dcd3a17549345ad9a6356297/ros-satnogs.png)

This ros-package is tested with:
* ROT_MODEL_DUMMY
* ROT_MODEL_EASYCOMM3
* ROT_MODEL_EASYCOMM2

## License
Licensed under the [GPLv3](LICENSE)
